# monstercat-rs
[Docs](https://ma_124.gitlab.io/monstercat-rs/monstercat/)

Rust bindings to Monstercats' `player.monstercat.app` API.
Not affiliated with or endorsed by Monstercat.

Bindings to the discontinued Connect API v2 can be found on the `master` branch.

## License
Copyright &copy; 2021 Ma_124
[License](./LICENSE)

