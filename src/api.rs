//! Interact with API endpoints.

use std::{sync::Arc, time::Duration};

use percent_encoding::{percent_encode, NON_ALPHANUMERIC};
use reqwest::{
    cookie::{CookieStore, Jar},
    Response, StatusCode, Url,
};
use serde::{de::DeserializeOwned, Deserialize, Serialize};

use crate::{
    paging::{Page, Paged},
    types::{Artist, Filters, Me, Playlist, Release, Track, Uuid},
    Error,
};

pub const ENDPOINT: &str = "https://player.monstercat.app/api";

#[derive(Debug, Clone, Copy)]
pub enum SignInResponse {
    /// Sign In Process is complete.
    Complete,

    /// A 2FA token is required to complete.
    ///
    /// Send token using [`Client::send_token`].
    SendToken,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct AuthToken {
    cookie: String,
}

impl AuthToken {
    fn cookie_domain() -> Url {
        Url::parse("https://player.monstercat.app/").unwrap()
    }

    fn add_to_jar(&self, cookies: &mut Jar) {
        cookies.add_cookie_str(&self.cookie, &Self::cookie_domain());
    }

    fn from_jar(cookies: &Jar) -> Option<Self> {
        let h = cookies.cookies(&Self::cookie_domain())?;
        let cookie = h.to_str().ok()?;
        Some(Self {
            cookie: cookie.to_owned(),
        })
    }
}

#[derive(Debug, Clone)]
pub struct Client {
    c: reqwest::Client,
    cookies: Arc<Jar>,
}

#[derive(Debug)]
pub struct ClientBuilder {
    cookies: Option<Jar>,
    b: reqwest::ClientBuilder,
}

impl ClientBuilder {
    pub fn with_auth_token(mut self, t: AuthToken) -> Self {
        let mut jar = Jar::default();
        t.add_to_jar(&mut jar);
        self.cookies = Some(jar);
        self
    }

    pub fn with_timeout(mut self, timeout: Duration) -> Self {
        self.b = self.b.timeout(timeout);
        self
    }

    pub fn build(self) -> Result<Client, Error> {
        let cookies = Arc::new(self.cookies.unwrap_or_default());
        Ok(Client {
            c: self
                .b
                .https_only(true)
                .cookie_provider(cookies.clone())
                .build()
                .map_err(reqw_error("constructing client"))?,
            cookies,
        })
    }
}

fn reqw_error(url: &str) -> impl Fn(reqwest::Error) -> Error + '_ {
    |err| Error::Api {
        url: url.to_owned(),
        err,
    }
}

impl Client {
    pub fn builder(botname: &str, version: &str, contact: &str) -> ClientBuilder {
        ClientBuilder {
            b: reqwest::ClientBuilder::new()
                .user_agent(format!("{}/{} (+{})", botname, version, contact)),
            cookies: None,
        }
    }

    pub fn auth_token(&self) -> Option<AuthToken> {
        AuthToken::from_jar(&self.cookies)
    }

    pub(crate) async fn get(&self, url: &str) -> Result<reqwest::Response, Error> {
        #[cfg(test)]
        println!("GET: {}", url);
        self.c
            .get(url)
            .send()
            .await
            .map_err(reqw_error(url))?
            .error_for_status()
            .map_err(reqw_error(url))
    }

    pub(crate) async fn get_json<T: DeserializeOwned>(&self, url: &str) -> Result<T, Error> {
        self.get(url).await?.json().await.map_err(reqw_error(url))
    }

    async fn post<B: Serialize>(&self, url: &str, body: Option<&B>) -> Result<StatusCode, Error> {
        let mut r = self.c.post(url);
        if let Some(body) = body {
            r = r.json(body)
        }

        Ok(r.send()
            .await
            .map_err(reqw_error(url))?
            .error_for_status()
            .map_err(reqw_error(url))?
            .status())
    }
}

impl Client {
    pub async fn artist_by_slug(&self, slug: &str) -> Result<Artist, Error> {
        self.get_json(&format!("{}/artist/{}", ENDPOINT, slug))
            .await
    }

    pub async fn filters(&self) -> Result<Filters, Error> {
        self.get_json(&format!("{}/catalog/filters", ENDPOINT))
            .await
    }

    pub async fn me(&self) -> Result<Me, Error> {
        self.get_json(&format!("{}/me", ENDPOINT)).await
    }

    pub async fn playlist_by_id(&self, id: &Uuid) -> Result<Playlist, Error> {
        let w: PlaylistWrapper = self
            .get_json(&format!("{}/playlist/{}", ENDPOINT, id))
            .await?;
        Ok(w.playlist)
    }

    pub fn tracks_in_playlist(
        &self,
        id: &Uuid,
    ) -> Paged<Track, impl DeserializeOwned + Into<Page<Track>>> {
        Paged::<_, TransparentWrapper<_>>::new(
            self.clone(),
            format!("{}/playlist/{}/catalog?", ENDPOINT, id),
        )
    }

    pub fn tracks_by_mood(
        &self,
        mood: &str,
    ) -> Paged<Track, impl DeserializeOwned + Into<Page<Track>>> {
        Paged::<_, TransparentWrapper<_>>::new(
            self.clone(),
            format!("{}/catalog/mood/{}?", ENDPOINT, mood),
        )
    }

    pub fn tracks<'a>(
        &self,
        opts: &SearchOptions<'a>,
    ) -> Paged<Track, impl DeserializeOwned + Into<Page<Track>>> {
        Paged::<_, TransparentWrapper<_>>::new(self.clone(), opts.url("catalog/browse"))
    }

    pub fn releases<'a>(
        &self,
        opts: &SearchOptions<'a>,
    ) -> Paged<Release, impl DeserializeOwned + Into<Page<Release>>> {
        Paged::<_, ReleasesWrapper>::new(self.clone(), opts.url("releases"))
    }

    pub fn artists<'a>(
        &self,
        opts: &SearchOptions<'a>,
    ) -> Paged<Artist, impl DeserializeOwned + Into<Page<Artist>>> {
        Paged::<_, ArtistsWrapper>::new(self.clone(), opts.url("artists"))
    }

    /// URL of a human friendly page describing the release.
    ///
    /// Example: <https://www.monstercat.com/release/MCEP136>
    pub fn release_page_url(&self, catalog_id: &str) -> String {
        format!("https://www.monstercat.com/release/{}", catalog_id)
    }

    /// URL of raw cover.
    ///
    /// You can resize and convert the image to other formats by using [`Client::cover_url`].
    pub fn raw_cover_url(&self, catalog_id: &str) -> String {
        format!("https://www.monstercat.com/release/{}/cover", catalog_id)
    }

    pub async fn download_raw_cover_reqwest(&self, catalog_id: &str) -> Result<Response, Error> {
        self.get(&self.raw_cover_url(catalog_id)).await
    }

    /// URL of a resized/reformatted version.
    ///
    /// Known formats are:
    /// - `"webp"`
    pub fn cover_url(&self, catalog_id: &str, width: u64, encoding: &str) -> String {
        format!("https://cdx.monstercat.com/?width={}&encoding={}&url=https%3A%2F%2Fwww.monstercat.com%2Frelease%2F{}%2Fcover", width, encoding, catalog_id)
    }

    pub async fn download_cover_reqwest(
        &self,
        catalog_id: &str,
        width: u64,
        encoding: &str,
    ) -> Result<Response, Error> {
        self.get(&self.cover_url(catalog_id, width, encoding)).await
    }

    /// URL of the track.
    ///
    /// Known formats:
    /// - `"mp3_320"`
    /// - `"flac"`
    /// - `"wav"`
    ///
    /// You can no longer download whole releases as ZIPs like you could with the Connect API.
    pub fn track_download_url(&self, release: &Uuid, track: &Uuid, format: &str) -> String {
        format!(
            "{}/release/{}/track-download/{}?format={}",
            ENDPOINT, release, track, format
        )
    }

    pub async fn download_track_reqwest(
        &self,
        release: &Uuid,
        track: &Uuid,
        format: &str,
    ) -> Result<Response, Error> {
        self.get(&self.track_download_url(release, track, format))
            .await
    }

    // TODO the login API behaves differently in the browser but even in the browser it's weird:
    // POST https://player.monstercat.app/api/sign-in       {...} -> 209 {"StatusCode":209,"Name":"Error","Message":"Enter the code sent to your device","Errors":null}
    // POST https://player.monstercat.app/api/sign-in             -> 400 {"StatusCode":400,"Name":"Error","Message":"Invalid email or password provided.","Errors":null}
    // POST https://player.monstercat.app/api/sign-in/token {...} -> 204
    // POST https://player.monstercat.app/api/sign-in/token       -> 500 {"StatusCode":500,"Name":"Error","Message":"Missing token","Errors":null}

    pub async fn sign_in(&self, email: &str, password: &str) -> Result<SignInResponse, Error> {
        #[derive(Serialize)]
        #[serde(rename_all = "PascalCase")]
        struct Request<'a> {
            email: &'a str,
            password: &'a str,
        }

        match self
            .post(
                &format!("{}/sign-in", ENDPOINT),
                Some(&Request { email, password }),
            )
            .await?
            .as_u16()
        {
            // TODO probably just 209 but see the issue described above
            200 | 209 => Ok(SignInResponse::SendToken),
            _ => Ok(SignInResponse::Complete),
        }
    }

    // TODO should check error but see the issue described above
    #[allow(unused_must_use)]
    pub async fn send_token(&self, token: &str) -> Result<(), Error> {
        #[derive(Serialize)]
        #[serde(rename_all = "PascalCase")]
        struct Request<'a> {
            token: &'a str,
        }

        self.post(
            &format!("{}/sign-in/token", ENDPOINT),
            Some(&Request { token }),
        )
        .await;
        Ok(())
    }

    // `https://player.monstercat.app/api/sign-in/send-token` currently responds with 404.
    // It's supposed to send a push notification to the 2FA app.

    pub async fn sign_out(&self) -> Result<(), Error> {
        self.post::<Option<()>>("https://player.monstercat.app/api/sign-out", None)
            .await?;
        Ok(())
    }
}

#[derive(Debug, Clone)]
#[non_exhaustive]
pub struct SearchOptions<'a> {
    pub search: &'a str,

    pub artist_id: &'a Uuid,
    pub release_id: &'a Uuid,

    pub no_gold: bool,
    pub only_released: bool,

    pub types: Vec<&'static str>,
    pub brands: Vec<&'static str>,
    pub genres: Vec<&'static str>,
    pub tags: Vec<&'static str>,

    /// Sort results by some key.
    ///
    /// Search is ascending with `key` and descending with `-key`.
    ///
    /// Known keys for [`Track`]s:
    /// - [`"brand"`][Track::brand]
    /// - [`"bpm"`][Track::bpm]
    /// - `"date"`
    /// - [`"duration"`][Track::duration]
    /// - [`"genre"`][Track::genre_secondary] ([`Track::genre_secondary`])
    /// - [`"isrc"`][Track::isrc]
    /// - [`"title"`][Track::title]
    /// - [`"track_number"`][Track::track_number]
    ///
    /// Known keys for [`Release`]s:
    /// - [`"artist"`][Release::artists_title]
    /// - [`"date"`][Release::release_date]
    /// - [`"title"`][Release::title]
    /// - [`"brand"`][Release::brand]
    ///
    /// Known keys for [`Artist`]s:
    /// - [`"uri"`][Artist::slug] ([`Artist::slug`])
    /// - [`"name"`][Artist::name]
    pub sort: &'static str,
}

impl Default for SearchOptions<'_> {
    fn default() -> Self {
        Self {
            search: "",
            artist_id: &Uuid::NIL,
            release_id: &Uuid::NIL,
            no_gold: false,
            only_released: false,
            types: Vec::new(),
            brands: Vec::new(),
            genres: Vec::new(),
            tags: Vec::new(),
            sort: "",
        }
    }
}

fn push_bool(s: &mut String, k: &str, v: bool) {
    s.push_str(k);
    if v {
        s.push_str("true");
    } else {
        s.push_str("false");
    }
}

fn push_unescaped(s: &mut String, k: &str, v: &str) {
    if !v.is_empty() {
        s.push_str(k);
        s.push_str(v);
    }
}

fn push_escaped(s: &mut String, k: &str, v: &str) {
    if !v.is_empty() {
        s.push_str(k);
        s.push_str(&percent_encode(v.as_bytes(), NON_ALPHANUMERIC).to_string());
    }
}

fn push_uuid(s: &mut String, k: &str, v: &Uuid) {
    if !v.is_nil() {
        s.push_str(k);
        s.push_str(&v.to_string());
    }
}

impl<'a> SearchOptions<'a> {
    fn url(&self, ep: &'static str) -> String {
        let mut url = format!("{}/{}?", ENDPOINT, ep);

        push_escaped(&mut url, "&search=", self.search);

        push_uuid(&mut url, "&artistId=", self.artist_id);
        push_uuid(&mut url, "&releaseId=", self.release_id);

        push_bool(&mut url, "&nogold=", self.no_gold);
        push_bool(&mut url, "&onlyReleased=", self.only_released);

        for v in &self.types {
            push_unescaped(&mut url, "&types[]=", v)
        }

        for v in &self.brands {
            push_unescaped(&mut url, "&brands[]=", v)
        }

        for v in &self.genres {
            push_unescaped(&mut url, "&genres[]=", v)
        }

        for v in &self.tags {
            push_unescaped(&mut url, "&tags[]=", v)
        }

        push_unescaped(&mut url, "&sort=", self.sort);

        url
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
struct PlaylistWrapper {
    playlist: Playlist,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
struct ReleasesWrapper {
    releases: Page<Release>,
}

impl From<ReleasesWrapper> for Page<Release> {
    fn from(w: ReleasesWrapper) -> Self {
        w.releases
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
struct ArtistsWrapper {
    artists: Page<Artist>,
}

impl From<ArtistsWrapper> for Page<Artist> {
    fn from(w: ArtistsWrapper) -> Self {
        w.artists
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(transparent)]
struct TransparentWrapper<T>(Page<T>);

impl<T> From<TransparentWrapper<T>> for Page<T> {
    fn from(w: TransparentWrapper<T>) -> Self {
        w.0
    }
}
