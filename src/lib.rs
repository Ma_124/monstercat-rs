//! Rust bindings to Monstercats' player.monstercat.app API.

use err_derive::Error;

pub mod api;
mod macros;
pub mod paging;
pub mod types;
mod uuid;

/// Error type returned by this API.
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
    #[error(display = "api error: {}: {}", url, err)]
    Api {
        url: String,
        #[error(source, no_from)]
        err: reqwest::Error,
    },
}

#[cfg(test)]
mod test;
