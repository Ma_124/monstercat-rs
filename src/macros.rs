macro_rules! object {
	(
        $(#[$obj_attr:meta])*
        struct $obj_name:ident {
            $(
                $(#[$field_attr:meta])*
                $field_name:ident : $field_type:ty $(= $field_example:expr)?,
            )*
        }
    ) => {
            #[doc="`"]
            #[doc=stringify!($obj_name)]
            #[doc="` object.\n\n"]
            $(#[$obj_attr])*
            #[derive(Debug, Clone, Default, ::serde::Serialize, ::serde::Deserialize)]
            #[serde(rename_all = "PascalCase")]

            // require known and deny missing fields during tests
            //#[cfg_attr(debug_assertions, serde(deny_unknown_fields))]
            #[cfg_attr(not(debug_assertions), serde(default))]
            pub struct $obj_name {
                $(
                    $(#[$field_attr])*
                    $(
                        #[doc="Example: `"]
                        #[doc=stringify!($field_example)]
                        #[doc="`"]
                    )?
                    #[serde(deserialize_with = "deserialize_null_default")]
                    pub $field_name: $field_type,
                )*
            }
    };
}

pub(crate) use object;
