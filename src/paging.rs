//! Handle paging.

use std::marker::PhantomData;

use serde::{de::DeserializeOwned, Deserialize, Serialize};

use crate::{api::Client, Error};

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
//#[cfg_attr(debug_assertions, serde(deny_unknown_fields))]
pub struct Page<T> {
    pub data: Vec<T>,
    pub total: u64,
    pub limit: u64,
    pub offset: u64,
    // not_found
}

#[derive(Debug)]
pub struct Paged<T, W> {
    client: Client,
    url: String,
    limit: Option<u64>,
    offset: u64,
    typ: PhantomData<T>,
    wrapper: PhantomData<W>,

    first_page: Option<Page<T>>,
    potential_total: Option<u64>,
}

impl<T, W: DeserializeOwned + Into<Page<T>>> Paged<T, W> {
    /// How much the API returns at most
    const APPROX_PER_REQUEST_LIMIT: u64 = 50;

    /// Sometimes the API returns fewer than requested elements by a constant.
    /// E.g. request 20 get 17, request 23 get 20
    /// This tries to correct for that.
    const PER_REQUEST_LIMIT_DEVIATION: u64 = 5;

    /// `url` must contain a `?` to start the query part and must not neither `&offset=` nor `&limit=` set.
    pub(crate) fn new(client: Client, mut url: String) -> Self {
        debug_assert!(url.contains('?'));
        url.push_str("&offset=");
        Self {
            client,
            url,
            limit: None,
            offset: 0,
            typ: PhantomData::default(),
            wrapper: PhantomData::default(),
            first_page: None,
            potential_total: None,
        }
    }

    pub fn limit(mut self, limit: u64) -> Self {
        self.limit = Some(limit);
        self
    }

    pub fn offset(mut self, offset: u64) -> Self {
        self.offset = offset;
        self
    }

    async fn fetch_page(&self, offset: u64, limit: u64) -> Result<Page<T>, Error> {
        let offset = offset + self.offset;
        let url = format!(
            "{}{}&limit={}",
            self.url,
            offset,
            limit + Self::PER_REQUEST_LIMIT_DEVIATION,
        );
        let w: W = self.client.get_json(&url).await?;
        Ok(w.into())
    }

    fn size_aux(&self, potential_total: u64, limit: Option<u64>) -> u64 {
        if let Some(limit) = limit {
            if limit < potential_total - self.offset {
                limit
            } else {
                potential_total - self.offset
            }
        } else {
            potential_total - self.offset
        }
    }

    pub async fn size(&mut self) -> Result<u64, Error> {
        if let Some(potential_total) = self.potential_total {
            Ok(self.size_aux(potential_total, self.limit))
        } else {
            let page = self
                .fetch_page(
                    0,
                    self.limit
                        .unwrap_or(Self::APPROX_PER_REQUEST_LIMIT)
                        .min(Self::APPROX_PER_REQUEST_LIMIT),
                )
                .await?;
            let total = page.total;
            self.potential_total = Some(total);
            self.first_page = Some(page);
            Ok(self.size_aux(total, self.limit))
        }
    }

    pub async fn for_each_page(&mut self, mut f: impl FnMut(Vec<T>)) -> Result<(), Error> {
        let size = self.size().await? as usize;
        let mut got = 0;
        if let Some(first) = self.first_page.take().map(|p| p.data) {
            got += first.len();
            f(first);
        }
        while got < size {
            let page = self
                .fetch_page(
                    got as u64,
                    Self::APPROX_PER_REQUEST_LIMIT.min((size - got) as u64),
                )
                .await?;
            if page.data.is_empty() {
                // remote data shrunk during query
                break;
            }
            got += page.data.len();
            f(page.data);
        }
        Ok(())
    }

    pub async fn collect(&mut self) -> Result<Vec<T>, Error> {
        let size = self.size().await? as usize;
        let mut v = Vec::new();
        v.reserve(size + Self::PER_REQUEST_LIMIT_DEVIATION as usize);

        self.for_each_page(|mut vs| {
            if v.is_empty() {
                v = vs;
            } else {
                v.append(&mut vs);
            }
        })
        .await?;

        v.truncate(size);
        Ok(v)
    }
}
