use std::{
    env::{self, VarError},
    str::FromStr,
    time::Duration,
};

use crate::{
    api::{Client, SearchOptions, SignInResponse},
    types::{Artist, Release, Track, Uuid},
    Error,
};

const PER_REQUEST_LIMIT: u64 = 20;

fn client() -> Result<Client, Error> {
    Client::builder(
        "monstercat-rs tests",
        env!("CARGO_PKG_VERSION"),
        env!("CARGO_PKG_HOMEPAGE"),
    )
    .with_timeout(Duration::from_secs(10))
    .build()
}

fn assert_valid_release(_r: &Release, _s: &str) {
    // TODO tests on releases
}

fn assert_valid_artist(_r: &Artist, _s: &str) {
    // TODO tests on artists
}

fn assert_valid_track(t: &Track, s: &str) {
    assert!(!t.id.is_nil(), "{}: uuid", s);
    assert_eq!(t.isrc.len(), 12, "{}: isrc", s);
    assert!(!t.base_title.is_empty(), "{}: title", s);
    assert!(!t.artists_title.is_empty(), "{}: artists title", s);
    assert!(t.track_number > 0, "{}: track number", s);
    assert!(t.duration > 0, "{}: duration", s);
    assert!(t.bpm > 0.0, "{}: bpm", s);
    assert!(t.debut_date.is_some(), "{}: debut date", s);
    assert!(!t.brand.is_empty(), "{}: brand", s);
    assert!(t.brand_id > 0, "{}: brand id", s);
    assert!(!t.genre_primary.is_empty(), "{}: prim genre", s);
    assert!(!t.genre_secondary.is_empty(), "{}: sec genre", s);

    assert!(!t.artists.is_empty(), "{}: artists len", s);
    assert!(!t.artists[0].id.is_nil(), "{}: artist: id", s);
    assert!(!t.artists[0].slug.is_empty(), "{}: artist: slug", s);
    assert!(!t.artists[0].name.is_empty(), "{}: artist: name", s);
    assert!(!t.artists[0].role.is_empty(), "{}: artist: role", s);

    assert!(!t.release.id.is_nil(), "{}: uuid", s);
    assert!(!t.release.catalog_id.is_empty(), "{}: catalog id", s);
    assert!(!t.release.upc.is_empty(), "{}: upc", s);
    assert!(!t.release.artists_title.is_empty(), "{}: artists title", s);
}

const ARTIST_SLUG: &str = "pegboardnerds";
const ARTIST_UUID: &str = "4fe7b148-9f58-4cab-97e9-07654e099047";

#[tokio::test]
async fn artist_by_slug() -> Result<(), Error> {
    let client = client()?;
    let pegboardnerds = client.artist_by_slug(ARTIST_SLUG).await?;
    assert_eq!(pegboardnerds.id, ARTIST_UUID.parse().unwrap(), "uuid");
    assert_eq!(pegboardnerds.slug, ARTIST_SLUG, "slug");
    assert_eq!(pegboardnerds.name, "Pegboard Nerds", "name");
    assert!(pegboardnerds.years.contains(&2012), "years");
    assert!(!pegboardnerds.profile_file_id.is_nil(), "profile file id");
    assert!(!pegboardnerds.links.len() >= 6, "links");

    Ok(())
}

#[tokio::test]
async fn filters() -> Result<(), Error> {
    let client = client()?;
    let filters = client.filters().await?;
    assert!(filters.brands.len() >= 5, "brands");
    assert!(filters.genres.len() >= 20, "genres");
    assert_eq!(filters.types.len(), 3, "types");
    assert!(filters.tags.len() >= 20, "tags");
    Ok(())
}

const PLAYLIST_UUID: &str = "3ec52fe5-50c4-4e30-a529-9d7308e1eb1e";

#[tokio::test]
async fn playlist_by_id() -> Result<(), Error> {
    let client = client()?;
    let playlist = client
        .playlist_by_id(&PLAYLIST_UUID.parse().unwrap())
        .await?;
    assert_eq!(playlist.id, PLAYLIST_UUID.parse().unwrap(), "uuid");
    assert!(playlist.created_at.is_some(), "created_at");
    assert!(playlist.updated_at.is_some(), "updated_at");
    assert!(playlist.is_public, "public");
    assert!(!playlist.title.is_empty(), "title");
    assert!(playlist.num_records > 1, "# records");
    Ok(())
}

#[tokio::test]
async fn tracks_in_playlist() -> Result<(), Error> {
    let client = client()?;
    let tracks = client
        .tracks_in_playlist(&PLAYLIST_UUID.parse().unwrap())
        .limit(PER_REQUEST_LIMIT)
        .collect()
        .await?;
    assert!(tracks.len() > 1, "data");
    assert_valid_track(&tracks[0], "playlist");
    Ok(())
}

#[tokio::test]
async fn tracks_by_mood() -> Result<(), Error> {
    let client = client()?;
    let tracks = client
        .tracks_by_mood("chill")
        .limit(PER_REQUEST_LIMIT)
        .collect()
        .await?;
    assert!(tracks.len() > 1, "data");
    assert_valid_track(&tracks[0], "mood");
    Ok(())
}

#[test]
fn release_page_url() -> Result<(), Error> {
    assert_eq!(
        "https://www.monstercat.com/release/MCEP136",
        client()?.release_page_url("MCEP136")
    );
    Ok(())
}

#[test]
fn raw_cover_url() -> Result<(), Error> {
    assert_eq!(
        "https://www.monstercat.com/release/MCEP136/cover",
        client()?.raw_cover_url("MCEP136")
    );
    Ok(())
}

#[test]
fn cover_url() -> Result<(), Error> {
    assert_eq!(
        "https://cdx.monstercat.com/?width=600&encoding=webp&url=https%3A%2F%2Fwww.monstercat.com%2Frelease%2FMCEP136%2Fcover",
        client()?.cover_url("MCEP136", 600, "webp")
    );
    Ok(())
}

#[tokio::test]
async fn basic_tracks() -> Result<(), Error> {
    let client = client()?;
    let tracks = client
        .tracks(&SearchOptions::default())
        .limit(PER_REQUEST_LIMIT)
        .collect()
        .await?;
    assert!(tracks.len() > 1, "data");
    assert_valid_track(&tracks[0], "basic_tracks");
    Ok(())
}

#[tokio::test]
async fn basic_artists() -> Result<(), Error> {
    let client = client()?;
    let artists = client
        .artists(&SearchOptions::default())
        .limit(PER_REQUEST_LIMIT)
        .collect()
        .await?;
    assert!(artists.len() > 1, "data");
    assert_valid_artist(&artists[0], "basic_artists");
    Ok(())
}

#[tokio::test]
async fn basic_releases() -> Result<(), Error> {
    let client = client()?;
    let releases = client
        .releases(&SearchOptions::default())
        .limit(PER_REQUEST_LIMIT)
        .collect()
        .await?;
    assert!(releases.len() > 1, "data");
    assert_valid_release(&releases[0], "basic_releases");
    Ok(())
}

#[tokio::test]
async fn by_artist_releases() -> Result<(), Error> {
    let client = client()?;
    let artist_id = ARTIST_UUID.parse().unwrap();
    let releases = client
        .releases(&SearchOptions {
            artist_id: &artist_id,
            ..SearchOptions::default()
        })
        .limit(PER_REQUEST_LIMIT)
        .collect()
        .await?;
    assert!(releases.len() > 1, "data");
    assert_valid_release(&releases[0], "by_artist_releases");
    releases[0].artists_title.contains("Pegboard");
    Ok(())
}

#[tokio::test]
async fn test_paging() -> Result<(), Error> {
    let client = client()?;
    let releases = client
        .releases(&SearchOptions::default())
        .limit(150)
        .collect()
        .await?;
    assert!(releases.len() == 150, "data");
    for r in releases {
        assert_valid_release(&r, "test paging");
    }
    Ok(())
}

#[tokio::test]
async fn test_title_track() -> Result<(), Error> {
    let release_id = Uuid::from_str("5361c071-cf9b-49e5-b89e-723e51237a2c").unwrap();

    let client = client()?;
    let rs = client
        .releases(&SearchOptions {
            release_id: &release_id,
            ..SearchOptions::default()
        })
        .limit(1)
        .collect()
        .await?;
    assert_eq!(rs.len(), 1);
    assert_eq!(rs[0].base_title, "Deep in the Night");
    assert_eq!(rs[0].version, "Barely Alive Remix");
    assert_eq!(&rs[0].title(), "Deep in the Night Barely Alive Remix");

    let ts = client
        .tracks(&SearchOptions {
            release_id: &release_id,
            ..SearchOptions::default()
        })
        .limit(1)
        .collect()
        .await?;
    assert_eq!(ts.len(), 1);
    assert_eq!(ts[0].base_title, "Deep In The Night");
    assert_eq!(ts[0].version, "Barely Alive Remix");
    assert_eq!(&ts[0].title(), "Deep In The Night Barely Alive Remix");

    assert_eq!(ts[0].release.base_title, "Deep in the Night");
    assert_eq!(ts[0].release.version, "Barely Alive Remix");
    assert_eq!(
        &ts[0].release.title(),
        "Deep in the Night Barely Alive Remix"
    );

    Ok(())
}

#[tokio::test]
#[ignore]
async fn login() -> Result<(), Error> {
    let email = env::var("MONSTERCAT_MAIL").unwrap();
    let password = env::var("MONSTERCAT_PASSWORD").unwrap();
    let token = match env::var("MONSTERCAT_TOKEN") {
        Ok(t) => Some(t),
        Err(VarError::NotPresent) => None,
        Err(e) => panic!("cannot read MONSTERCAT_TOKEN: {}", e),
    };

    let client = client()?;
    assert!(client.me().await.is_err());

    let r = client.sign_in(&email, &password).await.expect("sign in");
    match r {
        SignInResponse::Complete => assert!(token.is_none(), "token was not required"),
        SignInResponse::SendToken => client
            .send_token(&token.expect("token is required"))
            .await
            .expect("sending token failed"),
    }

    let tok = client.auth_token();
    assert!(tok.is_some());
    println!(
        "{}",
        client
            .get("https://player.monstercat.app/api/me")
            .await?
            .text()
            .await
            .unwrap()
    );

    let me = client.me().await.expect("me failed when logged in");
    assert_eq!(me.user.email, email);

    let client = Client::builder(
        "monstercat-rs tests",
        env!("CARGO_PKG_VERSION"),
        env!("CARGO_PKG_HOMEPAGE"),
    )
    .with_timeout(Duration::from_secs(10))
    .with_auth_token(tok.unwrap())
    .build()?;

    let me = client
        .me()
        .await
        .expect("me failed when logged in with prev token");
    assert_eq!(me.user.email, email);
    // TODO more tests on me

    client.sign_out().await.expect("sign out failed");
    assert!(client.me().await.is_err());
    assert!(client.sign_out().await.is_ok());

    Ok(())
}

// TODO more browse tests
