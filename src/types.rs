//! Types returned by the API.

use std::borrow::Cow;

use crate::macros::object;
use chrono::Utc;
use serde::{Deserialize, Deserializer};

pub use crate::uuid::Uuid;

pub type DateTime = chrono::DateTime<Utc>;

object! {
    /// Returned at <https://player.monstercat.app/api/me>.
    struct Me {
        user: User,
        settings: UserSettings,
        // features
    }
}

object! {
    /// Part of [`Me`].
    /// Returned at <https://player.monstercat.app/api/me>.
    struct User {
        id: Uuid,
        updated_at: Option<DateTime>,
        settings: UserSettings,
        #[serde(rename = "PlayerUUID")]
        player_uuid: Uuid,
        my_library: Uuid,
        max_licenses: u64 = 0,
        created_at: Option<DateTime>,

        username: String = "Ma_124",
        pronouns: String = "",
        first_name: String = "",
        last_name: String = "",
        email: String = "info@example.org",
        birthday: Option<DateTime>,

        has_download: bool,
        has_gold: bool,
        has_password: bool,
        given_download_access: bool,
        // two_factor_pending_id
        two_factor_id: String = "12345678",
        auto_say_song: bool,
        say_song: bool,

        place_name: String = "Munich",
        place_name_full: String = "Munich, Germany",
        city: String = "Munich",
        prov_st: String = "BY",
        province_state: String = "Bavaria",
        country: String = "Germany",
        continent: String = "Europe",
        /// Coordinates of `place_name` (Central location in the city but not near my home): [Google Maps](https://goo.gl/maps/CYW7EyDYsXDn7oaq8)
        location_lng: f64 = 48.1351253,
        /// Coordinates of `place_name` (Central location in the city but not near my home): [Google Maps](https://goo.gl/maps/CYW7EyDYsXDn7oaq8)
        location_lat: f64 = 11.5819805,
        google_maps_place_id: String,

        // score: ?
        // free_gold_reason
        // free_gold
        // free_gold_at
        // attributes
    }
}

object! {
    /// Part of [`Me`] and [`User`].
    /// Returned at <https://player.monstercat.app/api/me>.
    struct UserSettings {
        auto_enable_streamer_mode: bool,
        preferred_format: String = "mp3_320",
        playlist_public_default: bool,
        block_unlicensable_tracks: bool,
        hide_unlicensable_tracks: bool,
    }
}

object! {
    /// Returned at <https://player.monstercat.app/api/artist/pegboardnerds>.
    struct Artist {
        id: Uuid,
        profile_file_id: Uuid,

        #[serde(rename = "URI")]
        slug: String = "pegboardnerds",
        name: String = "Pegboard Nerds",

        years: Vec<u64> = [2019, 2018, 2022],
        about: String = "",
        #[serde(rename = "FeaturedVideoURL")]
        featured_video_url: String = "",
        featured_release_id: String = "",
        links: Vec<SocialLink>,

        image_position_x: u64 = 0,
        image_position_y: u64 = 0,
    }
}

object! {
    /// Part of [`Artist`].
    /// Returned at <https://player.monstercat.app/api/artist/pegboardnerds>.
    struct SocialLink {
        #[serde(rename = "URL")]
        url: String = "YouTube",
        platform: String = "http://youtube.com/PegboardNerds",
    }
}

object! {
    /// Returned at <https://player.monstercat.app/api/catalog/filters>.
    struct Filters {
        tags: Vec<String> = ["Aggressive", "Ambient", "BadAss"],
        types: Vec<String> = ["Single", "EP", "Album"],
        genres: Vec<String> = ["DJ Tools", "Bass House", "Bass"],
        brands: Vec<Brand>,
    }
}

object! {
    /// Part of [`Filters`].
    /// Returned at <https://player.monstercat.app/api/catalog/filters>.
    struct Brand {
        title: String = "Uncaged",
        id: u64,
        active: bool,
    }
}

object! {
    /// Returned at:
    /// - <https://player.monstercat.app/api/catalog/browse?releaseId=980f7af8-ea08-4bbe-b236-499a8e08a051>
    /// - <https://player.monstercat.app/api/playlist/991334fb-ca5e-48c6-bc73-cb83c364357d/catalog>
    /// - <https://player.monstercat.app/api/catalog/mood/chill>
    struct Track {
        id: Uuid,
        #[serde(rename = "ISRC")]
        isrc: String = "CA6D21800183",

        artists_title: String = "Pegboard Nerds",
        #[serde(rename = "Title")]
        base_title: String = "Purple People Eater",
        version: String = "",
        track_number: u64 = 1,
        duration: u64 = 262,
        #[serde(rename = "BPM")]
        bpm: f64 = 160,
        debut_date: Option<DateTime>,

        brand: String = "Uncaged",
        brand_id: u64 = 1,

        explicit: bool,
        streamable: bool,
        downloadable: bool,
        in_early_access: bool,
        creator_friendly: bool,

        genre_primary: String = "Dance",
        genre_secondary: String = "Dubstep",
        tags: Vec<String> = [],

        playlist_sort: u64 = 0,

        artists: Vec<ArtistRef>,
        release: ReleaseRef,
    }
}

object! {
    /// Part of [`Track`].
    struct ArtistRef {
        id: Uuid,
        #[serde(rename = "URI")]
        slug: String = "pegboardnerds",
        name: String = "Pegboard Nerds",
        public: bool = true,
        /// All Roles (in Feb. 22):
        /// - `"Primary"`
        /// - `"Featured"`
        /// - `"Remixer"`
        role: String = "Primary",
    }
}

object! {
    /// Part of [`Track`].
    struct ReleaseRef {
        id: Uuid,
        catalog_id: String = "MCEP136-1",

        #[serde(rename = "UPC")]
        upc: String = "859727663018",

        artists_title: String = "Pegboard Nerds",
        #[serde(rename = "Title")]
        base_title: String = "Purple People Eater",
        version: String = "",
        description: String = "",

        public: bool = true,
        approved: bool = true,

        tags: Vec<String> = [],
    }
}

object! {
    struct Release {
        id: Uuid,
        catalog_id: String = "MCEP136-1",

        artists_title: String = "Pegboard Nerds",
        #[serde(rename = "Title")]
        base_title: String = "Purple People Eater",
        version: String = "",

        genre_primary: String = "Dance",
        genre_secondary: String = "Dubstep",

        streamable: bool = true,
        downloadable: bool = false,
        in_early_access: bool = false,

        release_date: Option<DateTime>,
        #[serde(rename = "Type")]
        r_type: String = "Single",
        brand: String = "Uncaged",
        youtube_url: String = "",
        description: String = "",

        // links
    }
}

object! {
    /// Returned at:
    /// - <https://player.monstercat.app/api/playlist/991334fb-ca5e-48c6-bc73-cb83c364357d>
    /// - <https://player.monstercat.app/api/playlists?mylibrary=1>
    struct Playlist {
        id: Uuid,
        user_id: Uuid,
        tile_file_id: Uuid,
        background_file_id: Uuid,

        title: String = "Top 30",
        description: String = "Listen to hit after hit with the ultimate list featuring the very best Monstercat has to offer!",
        num_records: u64 = 30,

        created_at: Option<DateTime>,
        updated_at: Option<DateTime>,

        is_public: bool = true,
        archived: bool = false,
        my_library: bool = false,

        // items
    }
}

impl Track {
    pub fn title(&self) -> Cow<'_, str> {
        if self.version.is_empty() {
            Cow::from(&self.base_title)
        } else {
            Cow::from(format!("{} {}", &self.base_title, &self.version))
        }
    }
}

impl Release {
    pub fn title(&self) -> Cow<'_, str> {
        if self.version.is_empty() {
            Cow::from(&self.base_title)
        } else {
            Cow::from(format!("{} {}", &self.base_title, &self.version))
        }
    }
}

impl ReleaseRef {
    pub fn title(&self) -> Cow<'_, str> {
        if self.version.is_empty() {
            Cow::from(&self.base_title)
        } else {
            Cow::from(format!("{} {}", &self.base_title, &self.version))
        }
    }
}

fn deserialize_null_default<'de, D, T>(deserializer: D) -> Result<T, D::Error>
where
    T: Default + Deserialize<'de>,
    D: Deserializer<'de>,
{
    Ok(Option::deserialize(deserializer)?.unwrap_or_default())
}
