use std::{fmt::Debug, fmt::Display, str::FromStr};

use serde::{de, Deserialize, Deserializer, Serialize};

/// A UUID.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
pub struct Uuid(uuid::Uuid);

impl Uuid {
    /// The NIL UUID.
    pub const NIL: Uuid = Uuid(uuid::Uuid::nil());

    /// Tests if the UUID is nil.
    pub fn is_nil(&self) -> bool {
        self.0.is_nil()
    }

    /// Returns the UUID as big-endian bytes.
    pub fn as_bytes(&self) -> &[u8; 16] {
        self.0.as_bytes()
    }

    /// Creates a UUID using the supplied big-endian bytes.
    pub fn from_bytes(uuid: [u8; 16]) -> Self {
        Self(uuid::Uuid::from_bytes(uuid))
    }
}

impl Display for Uuid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Debug for Uuid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl FromStr for Uuid {
    type Err = ParseUuidError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        uuid::Uuid::parse_str(s).map(Uuid).map_err(ParseUuidError)
    }
}

impl<'de> Deserialize<'de> for Uuid {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        fn de_error<E: de::Error>(e: impl Display) -> E {
            E::custom(format_args!("UUID parsing failed: {}", e))
        }

        let s = String::deserialize(deserializer)?;
        if s.is_empty() {
            Ok(Uuid::NIL)
        } else {
            s.parse().map_err(de_error)
        }
    }

    fn deserialize_in_place<D>(deserializer: D, place: &mut Self) -> Result<(), D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        // Default implementation just delegates to `deserialize` impl.
        *place = Deserialize::deserialize(deserializer)?;
        Ok(())
    }
}

impl Serialize for Uuid {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.0.serialize(serializer)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ParseUuidError(<uuid::Uuid as FromStr>::Err);

impl Display for ParseUuidError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
